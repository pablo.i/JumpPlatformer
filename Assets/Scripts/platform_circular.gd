
extends StaticBody2D

var velocity = Vector2(0, 0)
var rotate_speed = 1
var radius = 100
var centre = null # hacer get_pos() en ready
var angle = 0

func _ready():
	set_process(true)
	centre = get_pos()

func circle_move():
	centre += velocity * get_fixed_process_delta_time()
	angle += rotate_speed * get_fixed_process_delta_time()
	var offset = Vector2(sin(angle), cos(angle)) * radius
	set_pos(centre + offset)

func _process(delta):
	circle_move()