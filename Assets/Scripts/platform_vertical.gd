
extends StaticBody2D
var max_height = -1.0
onready var timer = get_node("Timer")

func _ready():
	timer.set_wait_time(2.0)
	timer.start()
	set_process(true)

func _process(delta):
	move_local_y( 1 * max_height)

func _on_Timer_timeout():
	max_height = max_height * -1
	timer.start()