
extends Node2D

onready var player = get_node("Player")

func _ready():
	set_process(true)
	get_node("StreamPlayer").play()
	
func _process(delta):
	if Input.is_key_pressed(82):
		get_tree().reload_current_scene()
	if Input.is_key_pressed(81):
		get_tree().quit()
		
	if player.get_pos().y > 558:
		get_tree().change_scene("res://gameOver.xml")